<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Events\ChatEvent;
// use App\Http\Controllers\ChatEvent;
use App\User;

class chatController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function chat()
    {
    	return view('welcome');
    } 
    public function send(request $request)
    {
    	 return $request->all();
    	$user=User::find(Auth::id());
    	event(new ChatEvent($request->$message,$user));
    }
    // public function send()
    // {
    // 	return $request->all();
    // 	$user=User::find(Auth::id());
    // 	event(new ChatEvent($message,$user));
    // }
}
