<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>LARA CHAT</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link href="/css/app.css" rel="stylesheet">

        <!-- Styles -->
       <style>
           .all-message
           {
            height: 400px;
            overflow-y: scroll;
           }
       </style>
    </head>
    <body>
       <div id="app">
           <div class="container col-md-4">
              <li class="list-group-item list-group-item-primary">Lara Chat</li>

            <ul class="list-group all-message" v-chat-scroll>
   <message v-for="value in chat.message" v-bind:key="value.id" color='warning'>@{{ value }}</message>
</ul>
  <input type="text" class="form-control" placeholder="Type Your Message" v-model='message' @keyup.enter='send'>
            
           </div>
       </div>



       <script src="{{asset('/js/app.js')}}"></script>
    </body>
</html>
